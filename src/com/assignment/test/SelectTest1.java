package com.assignment.test;

import static org.junit.Assert.assertEquals;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import com.assignment.entity.Users;

public class SelectTest1 {
	EntityManagerFactory fact = null;
	EntityManager em = null;
	
	@BeforeAll
	public void setup() {
		fact = Persistence.createEntityManagerFactory("unit1");	
		em = fact.createEntityManager();
	}
	@Test
	public void selectTest() {
		Users u = em.find(Users.class, 101);
		
		assertEquals(101, u.getCid());
		assertEquals("abc", u.getCname());
		assertEquals("abc@gmail.com", u.getCmail());
	}
	
		
	@AfterAll
	public void close() {
		em.close();
		fact.close();
	}
}
