package com.assignment.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.assignment.entity.Users;

public class Delete1 {
	public static void main(String[] args) {
		EntityManagerFactory fact = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = fact.createEntityManager();
		
		em.getTransaction().begin();
		
		Users temp = em.find(Users.class, 101);
		if(temp != null ) {
			em.remove(temp);
		} else {
			System.out.println("Could not delete product as it is not available");
		}
		
		
		em.getTransaction().commit();
		em.close();
	}
}
