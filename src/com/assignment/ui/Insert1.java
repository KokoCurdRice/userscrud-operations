package com.assignment.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.assignment.entity.Users;

public class Insert1 {
	public static void main(String[] args) {
		EntityManagerFactory fact = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = fact.createEntityManager();
		
		em.getTransaction().begin();
		
		Users u = new Users(101, "abc", "abc@gmail.com");
		Users u1 = new Users(102, "def", "def@gmail.com");
		Users u2 = new Users(103, "ghi", "ghi@gmail.com");
		em.persist(u);
		em.persist(u1);
		em.persist(u2);
		
		
		em.getTransaction().commit();
		em.close();
		fact.close();
		
	}
}
