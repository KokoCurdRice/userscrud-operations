package com.assignment.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.assignment.entity.Users;

public class Merge1 {
	public static void main(String[] args) {
		EntityManagerFactory fact = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = fact.createEntityManager();
		
		em.getTransaction().begin();
		Users u = new Users(104, "mno", "mno@gmail.com");
		
		em.merge(u);
		
		em.getTransaction().commit();
		
		em.close();
		fact.close();
	}
}
