package com.assignment.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.assignment.entity.Users;

public class Select1 {
	public static void main(String[] args) {
		EntityManagerFactory fact = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = fact.createEntityManager();
		
		Users temp = em.find(Users.class, 102);
		
		System.out.println(temp);
		
		em.close();
		fact.close();
	}
}
