package com.assignment.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import com.assignment.entity.Users;

public class Update1 {
	public static void main(String[] args) {
		EntityManagerFactory fact = Persistence.createEntityManagerFactory("unit1");
		EntityManager em = fact.createEntityManager();
		
		em.getTransaction().begin();
		Users temp = em.find(Users.class, 102);
		
		if(temp != null) {
		temp.setCname("jkl");
		temp.setCmail("jkl@gmail.com");
		}
		else {
			System.out.println("Could not update the product as it is not available");
		}
		em.getTransaction().commit();
		
		em.close();
		fact.close();
	}
}
